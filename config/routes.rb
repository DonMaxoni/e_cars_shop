Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  root 'pages#index'

  resources :electric_cars, only: [:show]
  resources :orders, only: [:new, :create]

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :electric_cars, only: [:index]
    end
  end
end
