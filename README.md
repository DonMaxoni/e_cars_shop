## Pre-requisites

- ruby 2.5.1
- `bundle install`
- `yarn install`
- `rails db:migrate`

## Starting project
- To start project with Webpack live-reloading, run: `foreman start -f Procfile.dev`
- To start project with on-demand loading, run: `bundle exec rails s`

## Testing
To run all specs:
- run `rspec spec/`

Test environment depends on chrome driver. Requirements:
- **chrome**: Install chrome/chromium browser to support feature specs execution.
- **chromedriver**: Mac OS: `brew install chromedriver`, Ubuntu: `apt-get install chromium-chromedriver` (additional steps maybe required during installation using Ubuntu or other Linux OS)
