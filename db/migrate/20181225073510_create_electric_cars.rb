class CreateElectricCars < ActiveRecord::Migration[5.2]
  def change
    create_table :electric_cars do |t|
      t.string :brand
      t.string :model
      t.string :colors
      t.text :specification
      t.text :description
      t.integer :price

      t.timestamps
    end
  end
end
