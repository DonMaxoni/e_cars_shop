module ElectricCarsHelper
  def electric_car_title(car)
    "#{car.brand} - #{car.model}"
  end
end
