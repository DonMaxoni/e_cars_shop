import 'whatwg-fetch';

class ElectricCars {
  static all () {
    return fetch('/api/v1/electric_cars', {
      method: 'GET',
      credentials: 'same-origin'
    }).then(response => {
      return response.json();
    });
  }
};

export default ElectricCars;
