import React from 'react';

function Loader (props) {
  const showLoader = props.showLoader;

  if (!showLoader) { return null }

  return (
    <div className="loader">
      <img src="/images/spinner.gif" />
    </div>
  )
}

export default Loader;
