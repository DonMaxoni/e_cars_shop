import React from 'react';

function CarItem (props) {
  const car = props.car;
  const carAttrs = car.attributes;
  const carLink = `/electric_cars/${car.id}`;
  const carTitle = `${carAttrs.brand} - ${carAttrs.model}`;

  return (
    <div className="car-item">
      <a href={ carLink } >
        <img className="car-item--img" src="http://placehold.it/700x400" alt="empty" />
      </a>

      <div className="car-item--body">
        <div className="car-item--title">
          <h4>
            <a href={ carLink } >{ carTitle }</a>
          </h4>

          <h4>{ carAttrs.price }</h4>
        </div>

        <p className="car-item--text">{ carAttrs.description }</p>
      </div>
    </div>
  )
}

export default CarItem;
