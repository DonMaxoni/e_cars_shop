import React from 'react';
import { useState, useEffect } from 'react';
import ElectricCars from '../api/electric_cars';
import CarItem from './car_item';
import Loader from './../common/loader';

function CarList () {
  const [cars, setCars] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect( () => {
    ElectricCars.all().then((json) => {
      setIsLoading(false);
      setCars(json.data);
    });
  }, []);

  const carItems = cars.map((car) => {
    return (
      <div key={ car.id } className="col-xs-12 col-sm-6 col-md-4">
        <CarItem car={ car } />
      </div>
    )
  });

  return (
    <div className="row car-list">
      <Loader showLoader={ isLoading } />
      { carItems }
    </div>
  )
};

export default CarList;
