import React from 'react';
import ReactDOM from 'react-dom';
import CarList from './car_list';

document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(
    <CarList />,
    document.body.querySelector('#react--car-list')
  );
});
