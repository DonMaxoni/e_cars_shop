class Api::V1::ElectricCarsController < ApplicationController
  def index
    @electric_cars = ElectricCar.all
    render :index, status: :ok
  end
end
