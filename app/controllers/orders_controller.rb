class OrdersController < ApplicationController
  def new
    @order = Order.new
  end

  def create
    @order = Order.new(order_params)

    if @order.save
      flash[:success] = 'Order has been successfully created. Our manager will contact you soon.'
      redirect_to electric_car_path(@order.electric_car_id)
    else
      flash[:alert] = 'Some of the order details are incorrect. '\
                      'Please update the form with correct details'
      render :new
    end
  end

  private

  def order_params
    params.require(:order).permit(:electric_car_id, :first_name, :last_name, :email, :phone_number)
  end
end
