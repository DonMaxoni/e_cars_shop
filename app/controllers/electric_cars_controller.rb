class ElectricCarsController < ApplicationController
  def show
    @electric_car = ElectricCar.find(params[:id])
  end
end
