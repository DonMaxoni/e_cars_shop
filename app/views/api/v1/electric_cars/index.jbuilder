json.links do
  json.self api_v1_electric_cars_path
end

json.data do
  json.array! @electric_cars do |car|
    json.type car.class.name
    json.id car.id

    json.links do
      json.self electric_car_path(car.id)
    end

    json.attributes do
      json.title electric_car_title(car)
      json.brand car.brand
      json.model car.model
      json.colors car.colors
      json.specification car.specification
      json.description truncate(car.description, length: 150)
      json.price number_to_currency(car.price_in_dollars)
    end
  end
end
