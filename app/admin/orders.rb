ActiveAdmin.register Order do
  permit_params :electric_car_id, :first_name, :last_name, :email, :phone_number
end
