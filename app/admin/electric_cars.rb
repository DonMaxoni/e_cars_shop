ActiveAdmin.register ElectricCar do
  permit_params do
    [:brand, :model, :colors, :specification, :description, :price]
  end
end
