class Order < ApplicationRecord
  belongs_to :electric_car

  validates :first_name, :last_name, :email, :phone_number,
            :electric_car_id, presence: true
end
