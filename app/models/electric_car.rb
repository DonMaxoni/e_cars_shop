class ElectricCar < ApplicationRecord
  has_many :orders

  validates :brand, :model, :colors, :specification,
            :description, :price, presence: true

  validates :brand, :model, length: { maximum: 50 }

  def price_in_dollars
    (price.to_d / 100).to_f
  end
end
