require 'rails_helper'

RSpec.describe ElectricCar, type: :model do

  it { is_expected.to validate_presence_of(:brand) }
  it { is_expected.to validate_presence_of(:model) }
  it { is_expected.to validate_presence_of(:colors) }
  it { is_expected.to validate_presence_of(:specification) }
  it { is_expected.to validate_presence_of(:description) }
  it { is_expected.to validate_presence_of(:price) }

  it { is_expected.to validate_length_of(:brand).is_at_most(50) }
  it { is_expected.to validate_length_of(:model).is_at_most(50) }

  describe 'factory' do
    let(:e_car) { FactoryBot.build(:electric_car) }

    it 'should build a valid factory' do
      expect(e_car).to be_valid
    end
  end

  describe '#price_in_dollars' do
    it 'returns float' do
      car = FactoryBot.build(:electric_car, price: 10050)
      expect(car.price_in_dollars).to be_instance_of(Float)
      expect(car.price_in_dollars).to eq(100.50)
    end
  end
end
