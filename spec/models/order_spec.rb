require 'rails_helper'

RSpec.describe Order, type: :model do
  describe 'validations' do
    it { is_expected.to validate_presence_of(:electric_car_id) }
    it { is_expected.to validate_presence_of(:first_name) }
    it { is_expected.to validate_presence_of(:last_name) }
    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to validate_presence_of(:phone_number) }
  end

  describe 'factory' do
    let(:order) do
      FactoryBot.build(:order,
                       electric_car: FactoryBot.create(:electric_car))
    end

    it 'should build valid a valid factory' do
      expect(order).to be_valid
    end
  end
end
