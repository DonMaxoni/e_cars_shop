require 'rails_helper'

RSpec.describe AdminUser, type: :model do
  describe 'validation' do
    let(:admin_user) { FactoryBot.build(:admin_user) }

    it 'should validate the record' do
      expect(admin_user).to be_valid
    end
  end
end
