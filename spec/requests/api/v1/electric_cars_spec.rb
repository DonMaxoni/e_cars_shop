require 'rails_helper'

describe Api::V1::ElectricCarsController do
  describe 'GET /api/v1/electric_cars.json' do
    it 'returns 200 status' do
      get '/api/v1/electric_cars.json'
      expect(response).to have_http_status(:ok)
    end

    context 'when there are no cars' do
      let(:parsed_body) { JSON.parse(response.body, { symbolize_names: true }) }

      subject! { get '/api/v1/electric_cars' }

      it 'returns object with data and links keys' do
        expect(parsed_body).to eq({
          links: { self: '/api/v1/electric_cars' },
          data:  []
        })
      end
    end

    context 'when there are cars' do
      let!(:car) { FactoryBot.create(:electric_car) }
      let(:parsed_body) { JSON.parse(response.body, { symbolize_names: true }) }

      subject! { get '/api/v1/electric_cars' }

      it 'returns array of all cars' do
        expect(parsed_body).to eq({
          links: {
            self: '/api/v1/electric_cars'
          },
          data: [
            {
              id:   car.id,
              type: car.class.name,
              links: {
                self: "/electric_cars/#{car.id}"
              },
              attributes: {
                title:         "#{car.brand} - #{car.model}",
                brand:         car.brand,
                model:         car.model,
                colors:        car.colors,
                price:         "$#{car.price_in_dollars}",
                description:   car.description,
                specification: car.specification
              }
            }
          ]
        })
      end
    end
  end
end
