require 'selenium/webdriver'

module Support
  module Capybara
    module Drivers
      module_function

      def register
        ::Capybara.register_driver :chrome do |app|
          ::Capybara::Selenium::Driver.new(app, browser: :chrome)
        end

        ::Capybara.register_driver :headless_chrome do |app|
          caps = Selenium::WebDriver::Remote::Capabilities.chrome(
              chromeOptions: { args: %w[headless disable-gpu window-size=1366,2048] }
          )
          ::Capybara::Selenium::Driver.new app, browser: :chrome, desired_capabilities: caps
        end

        ::Capybara::Screenshot.register_driver(:headless_chrome) do |driver, path|
          driver.browser.save_screenshot(path)
        end
      end
    end
  end
end
