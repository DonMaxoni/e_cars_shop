require 'rails_helper'

RSpec.describe ElectricCarsController, type: :controller do
  describe "GET #show" do
    let(:car) { FactoryBot.create(:electric_car) }

    it "returns http success" do
      get :show, params: { id: car.id }
      expect(response).to have_http_status(:success)
    end
  end
end
