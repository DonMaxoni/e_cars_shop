FactoryBot.define do
  factory :electric_car do
    brand { Faker::Vehicle.manufacture }
    model { Faker::Vehicle.model }
    colors { Faker::Vehicle.color }
    specification { Faker::Vehicle.transmission }
    description { Faker::Vehicle.car_type }
    price { Faker::Number.number(5) }
  end
end
