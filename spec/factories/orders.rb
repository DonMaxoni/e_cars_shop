FactoryBot.define do
  factory :order do
    electric_car
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    email { Faker::Internet.email }
    phone_number { Faker::PhoneNumber.cell_phone }
    comment { "Nice" }
  end
end
