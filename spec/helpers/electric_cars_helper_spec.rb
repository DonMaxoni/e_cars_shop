require 'rails_helper'

RSpec.describe ElectricCarsHelper, type: :helper do
  describe '#electric_car_title' do
    let(:car) { FactoryBot.build(:electric_car) }

    it 'returns electric car title combined from brand and model' do
      expect(helper.electric_car_title(car)).to eq("#{car.brand} - #{car.model}")
    end
  end
end
