require 'rails_helper'

feature 'Home Page' do
  scenario 'User can visit home path' do
    visit '/'
    expect(page).to have_link('E-Cars Shop')
    expect(page).to have_content('About Store')
  end

  context 'with cars' do
    let!(:car_1) { FactoryBot.create(:electric_car) }
    let!(:car_2) { FactoryBot.create(:electric_car) }

    scenario 'User can see electric car titles' do
      visit '/'
      expect(page).to have_link("#{car_1.brand} - #{car_1.model}")
      expect(page).to have_link("#{car_2.brand} - #{car_2.model}")
    end

    scenario 'User can see car prices' do
      visit '/'
      expect(page).to have_content("$#{car_1.price_in_dollars}")
      expect(page).to have_content("$#{car_2.price_in_dollars}")
    end

    scenario 'User can see car descriptions' do
      visit '/'
      expect(page).to have_content("#{car_1.description}")
      expect(page).to have_content("#{car_2.description}")
    end

    scenario 'User can navigate to the electric car show page' do
      visit '/'

      expect(page).to have_link("#{car_1.brand} - #{car_1.model}")
      click_link("#{car_1.brand} - #{car_1.model}")

      expect(page).to have_current_path("/electric_cars/#{car_1.id}")
    end
  end
end
