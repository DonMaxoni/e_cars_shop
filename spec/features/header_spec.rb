require 'rails_helper'

feature 'Header' do
  scenario 'User can navigate to home page clicking logo text' do
    visit '/'
    expect(page).to have_link('E-Cars Shop')

    click_link('E-Cars Shop')
    expect(page).to have_current_path('/')
  end
end
