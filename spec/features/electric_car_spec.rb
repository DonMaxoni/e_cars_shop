require 'rails_helper'

feature 'Electric Car' do
  let(:car) { FactoryBot.create(:electric_car) }

  scenario 'User can visit electric car\'s detailed page' do
    visit "/electric_cars/#{car.id}"
    expect(page).to have_current_path("/electric_cars/#{car.id}")

    expect(page).to have_content("Brand: #{car.brand}")
    expect(page).to have_content("Model: #{car.model}")
    expect(page).to have_content("$#{car.price_in_dollars}")
    expect(page).to have_content("Available Colors: #{car.colors}")
    expect(page).to have_content("Description: #{car.description}")
    expect(page).to have_content("Specification")
    expect(page).to have_content(car.specification)
  end

  scenario 'User can buy the car' do
    visit "/electric_cars/#{car.id}"

    expect(page).to have_link('Buy this Car')
    click_link('Buy this Car')

    expect(page).to have_current_path("/orders/new?electric_car_id=#{car.id}")
  end
end
