require 'rails_helper'

feature 'Submitting Order' do
  let(:car) { FactoryBot.create(:electric_car) }

  scenario 'User can submit an order for the particular car' do
    visit "/electric_cars/#{car.id}"
    click_link('Buy this Car')

    expect(page).to have_content('Please fill in the form to complete the order')
    fill_in :order_first_name, with: 'Harry'
    fill_in :order_last_name, with: 'Potter'
    fill_in :order_email, with: 'harry@potter.com'
    fill_in :order_phone_number, with: '063-123-4567'
    click_button 'Create Order'

    expect(page).to have_current_path("/electric_cars/#{car.id}")
    expect(page)
      .to have_content('Order has been successfully created. Our manager will contact you soon.')
  end
end
